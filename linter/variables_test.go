package linter

import (
	"fmt"
	"testing"

	"alpinelinux.org/go/atools/apkbuild"
	"github.com/MakeNowJust/heredoc/v2"
	"github.com/stretchr/testify/require"
)

func VariableTestCaseSingleViolation(
	testcase string,
	t *testing.T,
	checkFunc func(*apkbuild.Apkbuild) []LintingViolation,
	expectedViolation LintingViolation,
) {

	file := apkbuild.MakeFile(heredoc.Doc(testcase))

	apkbuild, err := apkbuild.Parse(file, "APKBUILD")
	require.NoError(t, err)

	violations := checkFunc(apkbuild)

	require.NotEmpty(t, violations, "there should at least be one violation")
	require.True(t, violations[0].Is(expectedViolation), "expected violation %s, got %s", expectedViolation.Code, violations[0].Code)
}

func VariableTestCaseNoViolations(
	testcase string,
	t *testing.T,
	checkFunc func(*apkbuild.Apkbuild) []LintingViolation,
) {
	file := apkbuild.MakeFile(heredoc.Doc(testcase))

	apkbuild, err := apkbuild.Parse(file, "APKBUILD")
	require.NoError(t, err)

	violations := checkFunc(apkbuild)

	require.Empty(t, violations, "unexpected violations received: %#v", violations)
}

func TestDefaultBuilddirValue(t *testing.T) {
	program := `
		pkgname=apackage
		pkgver=1.2.3
		builddir="$srcdir"/$pkgname-$pkgver
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckDefaultBuildDir,
		AL1,
	)
}

func TestPkgNameIsNotQuoted(t *testing.T) {
	program := `
		pkgname="apackage"
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckPkgNameIsNotQuoted,
		AL3,
	)
}

func TestPkgVersionIsNotQuoted(t *testing.T) {
	program := `
		pkgver="1.2.3"
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckPkgVersionIsNotQuoted,
		AL4,
	)
}

func TestGlobalVariableIsNotEmptyUnquoted(t *testing.T) {
	program := `
		makedepends=
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckGlobalVariableIsNotEmpty,
		AL5,
	)
}

func TestGlobalVariableIsNotEmptyQuoted(t *testing.T) {
	program := `
		makedepends=""
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckGlobalVariableIsNotEmpty,
		AL5,
	)
}

func TestGlobalVariableIsNotEmptyMultipleVariables(t *testing.T) {
	file := apkbuild.MakeFile(heredoc.Doc(`
		var1=""
		var2="value"
		var3=""
	`))

	apkbuild, err := apkbuild.Parse(file, "APKBUILD")
	require.NoError(t, err)

	violations := SortViolationsByLine(CheckGlobalVariableIsNotEmpty(apkbuild))

	require.Len(t, violations, 2)
	require.Contains(t, violations[0].String(), "var1", "description of violation 1 should contain 'var1'")
	require.Contains(t, violations[1].String(), "var3", "description of violation 1 should contain 'var3'")
}

func TestUseOfCustomVariables(t *testing.T) {
	program := `
		myvar=value
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckUseOfCustomVariables,
		AL6,
	)
}

func TestUseOfCustomVariablesInFunctionIsAllowed(t *testing.T) {
	program := `
		func() {
			myvar=value
		}
	`

	VariableTestCaseNoViolations(
		program,
		t,
		CheckUseOfCustomVariables,
	)
}

func TestUseOfCustomVariableWithUnderscorePrefixIsAllowed(t *testing.T) {
	program := `
		_myvar=value
	`

	VariableTestCaseNoViolations(
		program,
		t,
		CheckUseOfCustomVariables,
	)
}

func TestUseOfCustomVariableWithOfficialVariableIsAllowed(t *testing.T) {
	program := `
		pkgname=value
	`

	VariableTestCaseNoViolations(
		program,
		t,
		CheckUseOfCustomVariables,
	)
}

func TestPkgNameUppercase(t *testing.T) {
	program := `
		pkgname=MyPackage
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckPkgNameUppercase,
		AL14,
	)
}

func TestPkgNameUppercaseAllowsLowercase(t *testing.T) {
	program := `
		pkgname=mypackage
	`

	VariableTestCaseNoViolations(
		program,
		t,
		CheckPkgNameUppercase,
	)
}

func TestPkgRelInPkgVerWithDash(t *testing.T) {
	program := `
		pkgver=1.2-r1
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckPkgRelInPkgVer,
		AL15,
	)
}

func TestPkgRelInPkgVerWithUnderscore(t *testing.T) {
	program := `
		pkgver=1.2_r1
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckPkgRelInPkgVer,
		AL15,
	)
}

func TestPkgRelInPkgVerAllowsRC(t *testing.T) {
	program := `
		pkgver=1.2_rc1
	`

	VariableTestCaseNoViolations(
		program,
		t,
		CheckPkgRelInPkgVer,
	)
}

func TestBackticksUsage(t *testing.T) {
	program := "JOBS=`nproc`"

	VariableTestCaseSingleViolation(program, t, CheckBackticksUsage, AL25)
}

func TestBackticksUsageInFunction(t *testing.T) {
	program := "func() {\n	JOBS=`nproc`\n}"

	VariableTestCaseSingleViolation(program, t, CheckBackticksUsage, AL25)
}

func TestBackticksUsageComplex(t *testing.T) {
	program := "foo=\"abc `cat foobar`\""

	VariableTestCaseSingleViolation(program, t, CheckBackticksUsage, AL25)
}

func TestUnderscoreBuildDir(t *testing.T) {
	program := `
		_builddir="$scrdir/foobar"
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckUnderscoreBuildDir,
		AL26,
	)
}

func TestUnderscoreBuildDirWithBuildDir(t *testing.T) {
	program := `
		builddir="$scrdir/foobar"
	`

	VariableTestCaseNoViolations(
		program,
		t,
		CheckUnderscoreBuildDir,
	)
}

func TestLiteralIntegerIsQuotedDoubleQuotes(t *testing.T) {
	program := `
		foobar="1"
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckLiteralIntegerIsQuoted,
		AL28,
	)
}

func TestLiteralIntegerIsQuotedSingleQuotes(t *testing.T) {
	program := `
		foobar='1'
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckLiteralIntegerIsQuoted,
		AL28,
	)
}

func TestLiteralIntegerIsQuotedWithSpacesIsFine(t *testing.T) {
	program := `
		foobar="1 2"
	`

	VariableTestCaseNoViolations(
		program,
		t,
		CheckLiteralIntegerIsQuoted,
	)
}

func TestLiteralIntegerIsQuotedQuotesInQuotesIsFine(t *testing.T) {
	program := `
		foobar='"1"'
	`

	VariableTestCaseNoViolations(
		program,
		t,
		CheckLiteralIntegerIsQuoted,
	)
}

func TestLiteralIntegerIsQuotedUnquotedIsFine(t *testing.T) {
	program := `
		foobar=1
	`

	VariableTestCaseNoViolations(
		program,
		t,
		CheckLiteralIntegerIsQuoted,
	)
}

func TestPkgnameInSource(t *testing.T) {
	program := `
		source="$pkgname-$pkver.tar.gz::https://example.com/$pkgname-$pkgver.tar.gz"
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckPkgnameInSource,
		AL29,
	)
}

func TestPkgnameInSourceWithPkgNameOnlyInArchiveName(t *testing.T) {
	program := `
		source="$pkgname-$pkver.tar.gz::https://example.com/foobar-$pkgver.tar.gz"
	`

	VariableTestCaseNoViolations(
		program,
		t,
		CheckPkgnameInSource,
	)
}

func TestDoubleUnderOnVariables(t *testing.T) {
	program := `
		__myvar=val
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckDoubleUnderOnVariables,
		AL30,
	)
}

func TestDoubleUnderOnVariablesInFunction(t *testing.T) {
	t.Skip("Not yet implemented")
	program := `
		foo() {
			local __myvar=val
		}
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckDoubleUnderOnVariables,
		AL30,
	)
}

func TestVariableCapitalized(t *testing.T) {
	program := `
		MyVar=val
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckVariableCapitalized,
		AL31,
	)
}

func TestVariableCapitalizedInFunction(t *testing.T) {
	program := `
		foo() {
			MyVar=val
 		}
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckVariableCapitalized,
		AL31,
	)
}

func TestVariableCapitalizedIgnoresLowercaseVariables(t *testing.T) {
	program := `
		myvar=val
	`

	VariableTestCaseNoViolations(
		program,
		t,
		CheckVariableCapitalized,
	)
}

func TestUnnecessaryUseOfBraces(t *testing.T) {
	program := `
		myvar="abc/${var}/def"
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckUnnecessaryUseOfBraces,
		AL32,
	)
}

func TestUnnecessaryUseOfBracesWithValidBraces(t *testing.T) {
	program := `
		myvar="abc/${var}def"
	`

	VariableTestCaseNoViolations(
		program,
		t,
		CheckUnnecessaryUseOfBraces,
	)
}

func TestUnnecessaryUseOfBracesWithoutBraces(t *testing.T) {
	program := `
		myvar="abc/$var-def"
	`

	VariableTestCaseNoViolations(
		program,
		t,
		CheckUnnecessaryUseOfBraces,
	)
}

func TestUnnecessaryUseOfBracesWithLiteralAfterQuotes(t *testing.T) {
	t.Skip("Not imlemented")
	program := `
		myvar="abc/${def}"/ghi
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckUnnecessaryUseOfBraces,
		AL32,
	)
}

func TestUnnecessaryUseOfBracesWithVariableInFunction(t *testing.T) {
	program := `
		foo() {
			myvar="abc/${def}/ghi"
		}
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckUnnecessaryUseOfBraces,
		AL32,
	)
}

func TestCpanVariablesDepends(t *testing.T) {
	program := `
		cpandepends="foo"
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckCpanVariables,
		AL35,
	)
}

func TestCpanVariablesMakeDepends(t *testing.T) {
	program := `
		cpanmakedepends="foo"
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckCpanVariables,
		AL35,
	)
}

func TestCpanVariablesCheckDepends(t *testing.T) {
	program := `
		cpancheckdepends="foo"
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckCpanVariables,
		AL35,
	)
}

func TestOverwriteXflags(t *testing.T) {
	for _, name := range []string{"CFLAGS", "CPPFLAGS", "CXXFLAGS", "GOFLAGS"} {
		program := fmt.Sprintf(`
			export %s="abc"
		`, name)

		VariableTestCaseSingleViolation(
			program,
			t,
			CheckOverwriteXflags,
			AL36,
		)
	}
}

func TestOverwriteXflagsInsideFunction(t *testing.T) {
	for _, name := range []string{"CFLAGS", "CPPFLAGS", "CXXFLAGS", "GOFLAGS"} {
		program := fmt.Sprintf(`
			func() {
				export %s="abc"
			}
		`, name)

		VariableTestCaseSingleViolation(
			program,
			t,
			CheckOverwriteXflags,
			AL36,
		)
	}
}

func TestOverwriteXflagsCorrectlyDetectsVariable(t *testing.T) {
	program := `
		CFLAGS="$CFLAGSABC foo"
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckOverwriteXflags,
		AL36,
	)
}

func TestOverwriteXflagsCorrectlyDetectsMultipleAssignments(t *testing.T) {
	program := `
		CFLAGS="foo"
		CFLAGS="$CFLAGS bar"
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckOverwriteXflags,
		AL36,
	)
}

func TestOverwriteXflagsCorrect(t *testing.T) {
	for _, name := range []string{"CFLAGS", "CPPFLAGS", "CXXFLAGS", "GOFLAGS"} {
		program := fmt.Sprintf(`
			export %[1]s="$%[1]s abc"
		`, name)

		VariableTestCaseNoViolations(
			program,
			t,
			CheckOverwriteXflags,
		)
	}
}

func TestInvalidOption(t *testing.T) {
	program := `
		options="invalid"
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckInvalidOption,
		AL49,
	)
}

func TestInvalidOptionValid(t *testing.T) {
	program := `
		options="net"
	`

	VariableTestCaseNoViolations(
		program,
		t,
		CheckInvalidOption,
	)
}

func TestInvalidArch(t *testing.T) {
	program := `
		arch="invalid"
	`

	VariableTestCaseSingleViolation(program, t, CheckInvalidArch, AL57)
}

func TestInvalidArchValidAll(t *testing.T) {
	program := `
		arch="all"
	`

	VariableTestCaseNoViolations(program, t, CheckInvalidArch)
}

func TestInvalidArchValidNoarch(t *testing.T) {
	program := `
		arch="noarch"
	`

	VariableTestCaseNoViolations(program, t, CheckInvalidArch)
}

func TestInvalidArchValidArch(t *testing.T) {
	program := `
		arch="x86_64"
	`

	VariableTestCaseNoViolations(program, t, CheckInvalidArch)
}

func TestInvalidArchValidCombination(t *testing.T) {
	program := `
		arch="x86_64 x86"
	`

	VariableTestCaseNoViolations(program, t, CheckInvalidArch)
}

func TestInvalidArchValidArchNegated(t *testing.T) {
	program := `
		arch="all !riscv64"
	`

	VariableTestCaseNoViolations(program, t, CheckInvalidArch)
}

func TestInvalidOptionValidNegatedOption(t *testing.T) {
	program := `
		options="!check"
	`

	VariableTestCaseNoViolations(
		program,
		t,
		CheckInvalidOption,
	)
}

func TestInvalidOptionMixed(t *testing.T) {
	program := `
		options="net invalid"
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckInvalidOption,
		AL49,
	)
}

func TestBadVersion(t *testing.T) {
	program := `
		pkgver=invalid
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckBadVersion,
		AL61,
	)
}

func TestBadVersionWithValidVersion(t *testing.T) {
	program := `
		pkgver=1.2.3
	`

	VariableTestCaseNoViolations(
		program,
		t,
		CheckBadVersion,
	)
}

func TestVolatileSource(t *testing.T) {
	program := `
		source="https://github.com/example/project/commit/0123456789abcdef01234567890abcdef12345678.patch"
	`
	VariableTestCaseSingleViolation(
		program,
		t,
		CheckVolatileSource,
		AL62,
	)
}

func TestVolatileSourceRenamedPatch(t *testing.T) {
	program := `
		source="fix-build-issue.patch::https://github.com/example/project/commit/0123456789abcdef01234567890abcdef12345678.patch"
	`
	VariableTestCaseSingleViolation(
		program,
		t,
		CheckVolatileSource,
		AL62,
	)
}

func TestVolatileSourceMultipleSources(t *testing.T) {
	program := `
		source="
			https://github.com/example/project/archive/v1.2.3/archive-v1.2.3.tar.gz
			https://github.com/example/project/commit/0123456789abcdef01234567890abcdef12345678.patch"
	`
	VariableTestCaseSingleViolation(
		program,
		t,
		CheckVolatileSource,
		AL62,
	)
}

func TestVolatileSourceNormalSource(t *testing.T) {
	program := `
		source="https://github.com/example/project/archive/v1.2.3/archive-v1.2.3.tar.gz"
	`
	VariableTestCaseNoViolations(
		program,
		t,
		CheckVolatileSource,
	)
}

func TestVolatileSourceLocalPatch(t *testing.T) {
	program := `
		source="fix-build-issue.patch"
	`
	VariableTestCaseNoViolations(
		program,
		t,
		CheckVolatileSource,
	)
}
