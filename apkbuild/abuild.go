package apkbuild

import (
	"strings"
)

var (
	// A poor mans set
	OfficialVariables = map[string]bool{
		"HOSTCC":            true,
		"arch":              true,
		"builddir":          true,
		"checkdepends":      true,
		"depends":           true,
		"depends_dev":       true,
		"depends_doc":       true,
		"depends_libs":      true,
		"depends_openrc":    true,
		"depends_static":    true,
		"giturl":            true,
		"install":           true,
		"install_if":        true,
		"langdir":           true,
		"ldpath":            true,
		"license":           true,
		"linguas":           true,
		"makedepends":       true,
		"makedepends_build": true,
		"makedepends_host":  true,
		"md5sums":           true,
		"options":           true,
		"patch_args":        true,
		"pcprefix":          true,
		"pkgdesc":           true,
		"pkgdir":            true,
		"pkggroups":         true,
		"pkgname":           true,
		"pkgrel":            true,
		"pkgusers":          true,
		"pkgver":            true,
		"provider_priority": true,
		"provides":          true,
		"replaces":          true,
		"replaces_priority": true,
		"sha256sums":        true,
		"sha512sums":        true,
		"somask":            true,
		"sonameprefix":      true,
		"source":            true,
		"srcdir":            true,
		"startdir":          true,
		"subpackages":       true,
		"subpkgdir":         true,
		"triggers":          true,
		"url":               true,
	}

	DeprecatedVariables = map[string]struct{}{
		"cpandepends":      {},
		"cpanmakedepends":  {},
		"cpancheckdepends": {},
	}

	Options = map[string]struct{}{
		"!archcheck":       {},
		"!check":           {},
		"!dbg":             {},
		"!fhs":             {},
		"!spdx":            {},
		"!strip":           {},
		"!tracedeps":       {},
		"bigdocs":          {},
		"charset.alias":    {},
		"checkroot":        {},
		"chmod-clean":      {},
		"ldpath-recursive": {},
		"lib64":            {},
		"libtool":          {},
		"net":              {},
		"setcap":           {},
		"sover-namecheck":  {},
		"suid":             {},
		"textrels":         {},
		"toolchain":        {},
	}

	Arches = map[string]struct{}{
		"x86_64":  {},
		"x86":     {},
		"aarch64": {},
		"armv7":   {},
		"armhf":   {},
		"ppc64le": {},
		"s390x":   {},
		"riscv64": {},
		"mips":    {},
		"mips64":  {},
	}
)

func IsOfficialVariable(name string) bool {
	return OfficialVariables[name]
}

func IsDeprecatedVariable(name string) (exists bool) {
	_, exists = DeprecatedVariables[name]
	return
}

func HasCustomVariablePrefix(name string) bool {
	return strings.HasPrefix(name, "_")
}

func IsValidOption(option string) bool {
	_, exists := Options[option]
	return exists
}

func IsValidArch(arch string) bool {
	_, exists := Arches[arch]
	return exists
}
