package linter

import (
	"testing"

	"alpinelinux.org/go/atools/apkbuild"
	"github.com/MakeNowJust/heredoc/v2"
	"github.com/stretchr/testify/require"
)

func WhitespaceTestCaseSingleViolation(
	testcase string,
	t *testing.T,
	checkFunc func(*apkbuild.Apkbuild) []LintingViolation,
	expectedViolation LintingViolation,
) {
	t.Helper()
	file := apkbuild.MakeFile(heredoc.Doc(testcase))

	apkbuild, err := apkbuild.Parse(file, "APKBUILD")
	require.NoError(t, err)

	violations := checkFunc(apkbuild)

	require.NotEmpty(t, violations, "expted at least violation %s", expectedViolation.Code)
	require.True(t, violations[0].Is(expectedViolation), "expected violation %s, got %s", expectedViolation.Code, violations[0].Code)
}

func TestIndentTabs(t *testing.T) {
	program := `
	pkgname=my-pkg
	pkgver=1.2.3
	build() {
	    make
 	}
	`

	WhitespaceTestCaseSingleViolation(program, t, CheckIndentTabs, AL7)
}

func TestTrailingWhitespace(t *testing.T) {
	program := `pkgname=my-pkg `

	WhitespaceTestCaseSingleViolation(program, t, CheckTrailingWhitespace, AL8)
}
