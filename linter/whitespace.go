package linter

import (
	"strings"

	"alpinelinux.org/go/atools/apkbuild"
)

var (
	AL7 LintingViolation = LintingViolation{
		Code:        "AL7",
		Description: "indent with tabs",
		Certainty:   Certain,
		Severity:    Important,
		Skip:        "INDENT_TABS",
	}

	AL8 LintingViolation = LintingViolation{
		Code:        "AL8",
		Description: "trailing whitespace",
		Certainty:   Certain,
		Severity:    Important,
		Skip:        "TRAILING_WHITESPACE",
	}
)

func init() {
	Register(&AL7, CheckIndentTabs)
	Register(&AL8, CheckTrailingWhitespace)
}

func CheckIndentTabs(apkbuild *apkbuild.Apkbuild) (violations []LintingViolation) {
	for nr, line := range apkbuild.Lines {
		if strings.HasPrefix(line, "  ") {
			violations = append(violations, AL7.Instantiate(apkbuild.File.Name, uint(nr+1), 1))
		}
	}

	return
}

func CheckTrailingWhitespace(apkbuild *apkbuild.Apkbuild) (violations []LintingViolation) {
	for nr, line := range apkbuild.Lines {
		if strings.HasSuffix(line, " ") {
			violations = append(violations, AL8.Instantiate(apkbuild.File.Name, uint(nr+1), 1))
		}
	}

	return
}
