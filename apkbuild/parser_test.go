package apkbuild

import (
	"testing"

	"github.com/MakeNowJust/heredoc/v2"
	"github.com/stretchr/testify/require"
	"mvdan.cc/sh/v3/syntax"
)

func makeWordParts(program string) []syntax.WordPart {
	file := MakeFile(program)
	parsedFile, _ := syntax.NewParser().Parse(file, "APKBUILD")
	statement := parsedFile.Stmts[0]
	assigns := statement.Cmd.(*syntax.CallExpr).Assigns
	return assigns[0].Value.Parts
}

func makeWordPartsArgs(program string) []*syntax.Word {
	file := MakeFile(program)
	parsedFile, _ := syntax.NewParser().Parse(file, "APKBUILD")
	statement := parsedFile.Stmts[0]
	return statement.Cmd.(*syntax.CallExpr).Args
}

func TestParserContainsFileLines(t *testing.T) {
	p, err := Parse(MakeFile("a\nb\nc\nd\n"), "APKBUILD")
	require.NoError(t, err)

	require.Len(t, p.Lines, 5, "parser should contain 5 lines")
}

func TestParseWordPartsReturnsLiteral(t *testing.T) {
	want := "test"
	parts := []syntax.WordPart{&syntax.Lit{Value: want}}

	got, _ := parseWordParts(parts, VariableScope{})
	require.Equal(t, want, got)
}

func TestParseWordPartsReturnsDoubleQuoted(t *testing.T) {
	want := "test"
	parts := makeWordParts(`a=test`)

	got, _ := parseWordParts(parts, VariableScope{})

	require.Equal(t, want, got)
}

func TestParseWordPartsReturnsParamExp(t *testing.T) {
	want := "$test"
	parts := makeWordParts(`a="$test"`)

	got, _ := parseWordParts(parts, VariableScope{})

	require.Equal(t, got, want)
}

func TestParseWordPartsCanEvaluateVariables(t *testing.T) {
	variableValues := VariableScope{
		{Name: "pkgname", EvaluatedValue: "mypkg"},
		{Name: "pkgver", EvaluatedValue: "1.2.3"},
	}

	parts := makeWordParts(`builddir="$pkgname-$pkgver"`)

	want := "mypkg-1.2.3"
	_, got := parseWordParts(parts, variableValues)

	require.Equal(t, want, got)
}

func TestParserReturnsObject(t *testing.T) {
	file := MakeFile(heredoc.Doc(`
		true
	`))

	apkbuild, err := Parse(file, "APKBUILD")
	require.NoError(t, err)
	require.NotNil(t, apkbuild)
}

func TestParserFindGlobalVariables(t *testing.T) {
	file := MakeFile(heredoc.Doc(`
		a=1
		echo foo
		b="yes"
	`))

	apkbuild, err := Parse(file, "APKBUILD")
	require.NoError(t, err)

	require.Len(t, apkbuild.GlobalVariables, 2, "There should be 2 variables")
	varA := apkbuild.GlobalVariables.Get("a")
	require.True(t, varA.IsSome(), "Expect variable 'a' to be present")
	varA.IfSome(func(varA Variable) {
		require.Equal(t, "1", varA.Value, "Variable 'a' should equal to '1'")
		require.False(t, varA.Exported, "variable 'a' should not be exported")
	})

	varB := apkbuild.GlobalVariables.Get("b")
	require.True(t, varB.IsSome(), "Expect variable 'a' to be present")
	varB.IfSome(func(varB Variable) {
		require.Equal(t, "yes", varB.Value, "Variable 'b' should equal to 'yes'")
		require.False(t, varB.Exported, "variable 'b' should not be exported")
	})
}

func TestParseFindsGlobalVariableInIfStatement(t *testing.T) {
	file := MakeFile(heredoc.Doc(`
		if [ $a = 1 ]; then
			b=2
		fi
	`))

	apkbuild, err := Parse(file, "APKBUILD")
	require.NoError(t, err)

	varB := apkbuild.GlobalVariables.Get("b")
	require.True(t, varB.IsSome(), "Variable 'b' should exist")

	varB.IfSome(func(v Variable) {
		require.Equal(t, "2", v.EvaluatedValue)
	})
}

func TestParseFindsGlobalVariableInElseIfStatement(t *testing.T) {
	file := MakeFile(heredoc.Doc(`
		if [ $a = 1 ]; then
			b=2
		elif [ $a = 2 ]; then
			c=1
		fi
	`))

	apkbuild, err := Parse(file, "APKBUILD")
	require.NoError(t, err)

	varC := apkbuild.GlobalVariables.Get("c")
	require.True(t, varC.IsSome(), "Variable 'c' should exist")

	varC.IfSome(func(v Variable) {
		require.Equal(t, "1", v.EvaluatedValue)
	})
}

func TestParseFindsGlobalVariableInElseStatement(t *testing.T) {
	file := MakeFile(heredoc.Doc(`
		if [ $a = 1 ]; then
			b=2
		else
			c=1
		fi
	`))

	apkbuild, err := Parse(file, "APKBUILD")
	require.NoError(t, err)

	varC := apkbuild.GlobalVariables.Get("c")
	require.True(t, varC.IsSome(), "Variable 'c' should exist")

	varC.IfSome(func(v Variable) {
		require.Equal(t, "1", v.EvaluatedValue)
	})
}

func TestParseFindsGlobalVariableInCaseStatements(t *testing.T) {
	file := MakeFile(heredoc.Doc(`
		case $a in
			1|2|3) b=1;;
			4|5|6) c=1;;
		esac
	`))

	apkbuild, err := Parse(file, "APKBUILD")
	require.NoError(t, err)

	varB := apkbuild.GlobalVariables.Get("b")
	require.True(t, varB.IsSome(), "Variable 'b' should exist")
	varB.IfSome(func(v Variable) {
		require.Equal(t, "1", v.EvaluatedValue)
	})

	varC := apkbuild.GlobalVariables.Get("c")
	require.True(t, varC.IsSome(), "Variable 'c' should exist")
	varC.IfSome(func(v Variable) {
		require.Equal(t, "1", v.EvaluatedValue)
	})
}

func TestParseFindsGlobalVariableInForStatement(t *testing.T) {
	file := MakeFile(heredoc.Doc(`
		for i in 1 2 3; do
			b=2
		done
	`))

	apkbuild, err := Parse(file, "APKBUILD")
	require.NoError(t, err)

	varB := apkbuild.GlobalVariables.Get("b")
	require.True(t, varB.IsSome(), "Variable 'b' should exist")

	varB.IfSome(func(v Variable) {
		require.Equal(t, "2", v.EvaluatedValue)
	})
}

func TestParseFindsGlobalVariableInWhileStatement(t *testing.T) {
	file := MakeFile(heredoc.Doc(`
		while true; do
			b=2
		done
	`))

	apkbuild, err := Parse(file, "APKBUILD")
	require.NoError(t, err)

	varB := apkbuild.GlobalVariables.Get("b")
	require.True(t, varB.IsSome(), "Variable 'b' should exist")

	varB.IfSome(func(v Variable) {
		require.Equal(t, "2", v.EvaluatedValue)
	})
}

func TestParseFindsGlobalVariableInBraceBlock(t *testing.T) {
	file := MakeFile(heredoc.Doc(`
		{
			a=1
			b=2
		}
	`))

	apkbuild, err := Parse(file, "APKBUILD")
	require.NoError(t, err)

	varA := apkbuild.GlobalVariables.Get("a")
	require.True(t, varA.IsSome(), "Variable 'a' should exist")

	varA.IfSome(func(v Variable) {
		require.Equal(t, "1", v.EvaluatedValue)
	})

	varB := apkbuild.GlobalVariables.Get("b")
	require.True(t, varB.IsSome(), "Variable 'b' should exist")

	varB.IfSome(func(v Variable) {
		require.Equal(t, "2", v.EvaluatedValue)
	})
}

func TestParseFindsGlobalVariableWithDefault(t *testing.T) {
	t.Skipf("Not yet implemented")
	file := MakeFile(heredoc.Doc(`
		: ${FOO-bar}
	`))

	apkbuild, err := Parse(file, "APKBUILD")
	require.NoError(t, err)

	require.Len(t, apkbuild.GlobalVariables, 1, "FOO should be defined as a global variable")
}

func TestParserCanEvaluateGlobalVariables(t *testing.T) {
	file := MakeFile(heredoc.Doc(`
		var1=value
		var2="$var1"
	`))

	apkbuild, err := Parse(file, "APKBUILD")
	require.NoError(t, err)

	var2, err := apkbuild.GlobalVariables.Get("var2").Take()
	require.NoError(t, err, "var2 should exist as variable")

	require.Equal(t, "var2", var2.Name)

	got := var2.EvaluatedValue
	require.Equal(t, "value", got, "var2 should have the same value as var1")
}

func TestParserCanHandleChangesToVariables(t *testing.T) {
	file := MakeFile(heredoc.Doc(`
		var1=value
		var2="$var1"
		var1=anothervalue
		var3="$var1"
	`))

	apkbuild, err := Parse(file, "APKBUILD")
	require.NoError(t, err)

	var3, err := apkbuild.GlobalVariables.Get("var3").Take()
	require.NoError(t, err, "var3 should exist as variable")

	got := var3.EvaluatedValue
	require.Equal(t, "anothervalue", got, "var3 should be equal to the latest value of var1")
}

func testParserIgnoresVariablesInFunctionsForGlobalVariables(t *testing.T) {
	file := MakeFile(heredoc.Doc(`
		foo() {
			b=2
		}
	`))

	apkbuild, err := Parse(file, "APKBUILD")
	require.NoError(t, err)

	nrGlobalVariables := len(apkbuild.GlobalVariables)
	if nrGlobalVariables != 0 {
		t.Errorf("got %d variables, expected 0", nrGlobalVariables)
	}
}

func TestParserCanHandleEmptyVariables(t *testing.T) {
	file := MakeFile(heredoc.Doc(`
		myvar=
	`))

	apkbuild, err := Parse(file, "APKBUILD")
	require.NoError(t, err)

	require.Len(t, apkbuild.GlobalVariables, 1, "an empty variable should still be defined")
}

func TestParserHandlesExportedVariables(t *testing.T) {
	file := MakeFile(heredoc.Doc(`
		export MYVAR=1
	`))

	apkbuild, err := Parse(file, "APKBUILD")
	require.NoError(t, err)

	require.Len(t, apkbuild.GlobalVariables, 1, "GlobalVariables should contain exported variables")
	myvar := apkbuild.GlobalVariables.Get("MYVAR")
	require.True(t, myvar.IsSome(), "GlobalVariables should contain exported variable MYVAR")
	myvar.IfSome(func(myvar Variable) {
		require.True(t, myvar.Exported, "Expect MYVAR to be exported")
	})
}

func TestParserHandlesLocalVariableWithAssignment(t *testing.T) {
	file := MakeFile(heredoc.Doc(`
		func() {
			local a=1
		}
	`))

	apkbuild, err := Parse(file, "APKBUILD")
	require.NoError(t, err)

	require.Len(t, apkbuild.Functions, 1)
	f := apkbuild.Functions[0]

	varA := f.Variables.Get("a")
	require.True(t, varA.IsSome())
	varA.IfSome(func(varA Variable) {
		require.True(t, varA.Local, "Expect a to be local")
	})
}

func TestParserHandlesLocalVariableWithoutAssignment(t *testing.T) {
	file := MakeFile(heredoc.Doc(`
		func() {
			local a
		}
	`))

	apkbuild, err := Parse(file, "APKBUILD")
	require.NoError(t, err)

	require.Len(t, apkbuild.Functions, 1)
	f := apkbuild.Functions[0]

	varA := f.Variables.Get("a")
	require.True(t, varA.IsSome())
	varA.IfSome(func(varA Variable) {
		require.True(t, varA.Local, "Expect a to be local")
	})
}

func TestParserSkipsSingleCommandGlobalVariables(t *testing.T) {
	file := MakeFile(heredoc.Doc(`
		VERBOSE=1 command
	`))

	apkbuild, err := Parse(file, "APKBUILD")
	require.NoError(t, err)

	require.Len(t, apkbuild.GlobalVariables, 0, "Variables attached to command execution should not be considered a global variable")
}

func TestParserExtractsFunctions(t *testing.T) {
	program := MakeFile(heredoc.Doc(`
		myfunc() {
			echo myfunc
		}
	`))

	apkbuild, err := Parse(program, "APKBUILD")
	require.NoError(t, err)

	require.Len(t, apkbuild.Functions, 1)
	require.Equal(t, "myfunc", apkbuild.Functions[0].Name)
}

func TestParserExtractsVariablesFromFunctions(t *testing.T) {
	program := MakeFile(heredoc.Doc(`
		myfunc() {
			a=1
		}
	`))

	apkbuild, err := Parse(program, "APKBUILD")
	require.NoError(t, err)

	require.Len(t, apkbuild.Functions, 1)

	f := apkbuild.Functions[0]
	require.Len(t, f.Variables, 1)
	require.Equal(t, f.Variables[0].Name, "a")
	require.Equal(t, f.Variables[0].EvaluatedValue, "1")
}

func TestParserExtractsVariablesFromStatementsInFunctions(t *testing.T) {
	program := MakeFile(heredoc.Doc(`
		myfunc() {
			if [ "$b" = "a" ]; then
				a=1
			fi
		}
	`))

	apkbuild, err := Parse(program, "APKBUILD")
	require.NoError(t, err)

	require.Len(t, apkbuild.Functions, 1)

	f := apkbuild.Functions[0]
	require.Len(t, f.Variables, 1)
	require.Equal(t, f.Variables[0].Name, "a")
	require.Equal(t, f.Variables[0].EvaluatedValue, "1")
}

func TestParserExtractsCommandSubstitutions(t *testing.T) {
	program := MakeFile("a=$(abc)")

	apkbuild, err := Parse(program, "APKBUILD")
	require.NoError(t, err)

	varA := apkbuild.GlobalVariables.Get("a")
	require.True(t, varA.IsSome())

	varA.IfSome(func(varA Variable) {
		require.Len(t, varA.CommandSubst, 1)
	})
}

func TestParserExtractsCommandSubstitutionsComplex(t *testing.T) {
	program := MakeFile(`a="$(abc) def"`)

	apkbuild, err := Parse(program, "APKBUILD")
	require.NoError(t, err)

	varA := apkbuild.GlobalVariables.Get("a")
	require.True(t, varA.IsSome())

	varA.IfSome(func(varA Variable) {
		require.Len(t, varA.CommandSubst, 1)
		require.Equal(t, "$(abc) def", varA.Value)
		// We don't evaluate command substituions, we add them as is.
		require.Equal(t, "$(abc) def", varA.EvaluatedValue)
	})
}

func TestParserExtractsCommandSubstitutionsBackticks(t *testing.T) {
	program := MakeFile("a=`abc`")

	apkbuild, err := Parse(program, "APKBUILD")
	require.NoError(t, err)

	varA := apkbuild.GlobalVariables.Get("a")
	require.True(t, varA.IsSome())

	varA.IfSome(func(varA Variable) {
		require.Len(t, varA.CommandSubst, 1)
		require.True(t, varA.CommandSubst[0].Backquotes, "Backquotes should be true if backticks are used")
	})
}

func TestParseCallExprReturnsSimpleCommand(t *testing.T) {
	expr := &syntax.CallExpr{
		Args: makeWordPartsArgs(`make`),
	}

	call := ParseCallExpr(expr)

	require.Equal(t, "make", call.Command)
	require.Len(t, call.Args, 0)
}

func TestParseCallExprReturnsCommandWithArgs(t *testing.T) {
	expr := &syntax.CallExpr{
		Args: makeWordPartsArgs(`make with args`),
	}

	call := ParseCallExpr(expr)

	require.Equal(t, "make", call.Command)
	require.Len(t, call.Args, 2)
	require.Equal(t, "with", call.Args[0])
	require.Equal(t, "args", call.Args[1])
}

func TestParseCallExprReturnsCommandWithVariableInArgs(t *testing.T) {
	expr := &syntax.CallExpr{
		Args: makeWordPartsArgs(`cd $builddir`),
	}

	call := ParseCallExpr(expr)

	require.Equal(t, "cd", call.Command)
	require.Len(t, call.Args, 1)
	require.Equal(t, "$builddir", call.Args[0])
}
