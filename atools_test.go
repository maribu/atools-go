package atools_test

import (
	"os"
	"path"
	"testing"

	"alpinelinux.org/go/atools/linter"
	"github.com/stretchr/testify/require"
)

func AccTest(t *testing.T, testFile string, violationCode string) {
	t.Helper()

	f, err := os.Open(path.Join("testdata", testFile))
	require.NoError(t, err, "could not open testdata/%s", testFile)

	violations := linter.Check(f, testFile)
	require.Len(t, violations, 1)
	require.Equal(t, violationCode, violations[0].Code)
}

func TestFullGoodExample(t *testing.T) {
	testFile := "full_good_example.sh"
	f, err := os.Open(path.Join("testdata", testFile))
	require.NoError(t, err, "could not open testdata/%s", testFile)

	violations := linter.Check(f, testFile)
	require.Len(t, violations, 0)
}

func TestAccAL1DefaultBuilddirVariable(t *testing.T) {
	AccTest(t, "al01_default_builddir_value.sh", "AL1")
}

func TestAccAL2UnnecessaryReturn1(t *testing.T) {
	AccTest(t, "al02_unnecessary_return_1.sh", "AL2")
}

func TestAccAL3PkgnameQuoted(t *testing.T) {
	AccTest(t, "al03_pkgname_quoted.sh", "AL3")
}

func TestAccAL4PkgverQuoted(t *testing.T) {
	AccTest(t, "al04_pkgver_quoted.sh", "AL4")
}

func TestAccAL5EmptyVariable(t *testing.T) {
	AccTest(t, "al05_empty_variable.sh", "AL5")
}

func TestAccAL6CustomVariable(t *testing.T) {
	AccTest(t, "al06_custom_variable.sh", "AL6")
}

func TestAccAL7IndentTabs(t *testing.T) {
	AccTest(t, "al07_indent_tabs.sh", "AL7")
}

func TestAccAL8TrailingWhitespace(t *testing.T) {
	AccTest(t, "al08_trailing_whitespace.sh", "AL8")
}

func TestAccAL9FunctionKeyword(t *testing.T) {
	AccTest(t, "al09_function_keyword.sh", "AL9")
}

func TestAccAL10SpaceBeforeFunctionParenthesis(t *testing.T) {
	AccTest(t, "al10_space_before_function_parenthesis.sh", "AL10")
}

func TestAccAL11SpaceAfterFunctionParenthesis(t *testing.T) {
	AccTest(t, "al11_space_after_function_parenthesis.sh", "AL11")
}

func TestAccAL12NewlineOpeningBrace(t *testing.T) {
	AccTest(t, "al12_newline_opening_brace.sh", "AL12")
}

func TestAccAL13SuperfluousCdBuilddir(t *testing.T) {
	AccTest(t, "al13_superfluous_cd_builddir.sh", "AL13")
}

func TestAccAL13SuperfluousCdBuilddirCorrect(t *testing.T) {
	testFile := "al13_superfluous_cd_builddir_correct.sh"
	f, err := os.Open(path.Join("testdata", testFile))
	require.NoError(t, err, "could not open testdata/%s", testFile)

	violations := linter.Check(f, testFile)
	require.Len(t, violations, 0, "AL13 should not trigger when it's not the first cd")
}

func TestAccAL14PkgnameHasUppercase(t *testing.T) {
	AccTest(t, "al14_pkgname_has_uppercase.sh", "AL14")
}

func TestAccAL25BackticksUsage(t *testing.T) {
	AccTest(t, "al25_backticks_usage.sh", "AL25")
}

func TestAccAL26UnderBuilddirIsSet(t *testing.T) {
	AccTest(t, "al26_under_builddir_is_set.sh", "AL26")
}

func TestAccAL28LiteralIntegerIsQuoted(t *testing.T) {
	AccTest(t, "al28_literal_integer_is_quoted.sh", "AL28")
}

func TestAccAL29PkgnameUsedInSource(t *testing.T) {
	AccTest(t, "al29_pkgname_used_in_source.sh", "AL29")
}

func TestAccAL30DoubleUnderscoreInvariable(t *testing.T) {
	AccTest(t, "al30_double_underscore_in_variable.sh", "AL30")
}

func TestAccAL30DoubleUnderscoreInvariableFunc(t *testing.T) {
	AccTest(t, "al30_double_underscore_in_variable_func.sh", "AL30")
}

func TestAccAL30DoubleUnderscoreInvariableLocal(t *testing.T) {
	AccTest(t, "al30_double_underscore_in_variable_local.sh", "AL30")
}

func TestAccAL31VariableCapitalized(t *testing.T) {
	AccTest(t, "al31_variable_capitalized.sh", "AL31")
}

func TestAccAL32BracedVariable(t *testing.T) {
	AccTest(t, "al32_braced_variable.sh", "AL32")
}

func TestAccAL35CPANVariableCPANDepends(t *testing.T) {
	AccTest(t, "al35_cpan_variable_cpandepends.sh", "AL35")
}

func TestAccAL35CPANVariableCPANMakeDepends(t *testing.T) {
	AccTest(t, "al35_cpan_variable_cpanmakedepends.sh", "AL35")
}

func TestAccAL35CPANVariableCPANCheckDepends(t *testing.T) {
	AccTest(t, "al35_cpan_variable_cpancheckdepends.sh", "AL35")
}

func TestAccAL36OverwriteXflagsCflags(t *testing.T) {
	AccTest(t, "al36_overwrite_xflags_cflags.sh", "AL36")
}

func TestAccAL36OverwriteXflagsCppflags(t *testing.T) {
	AccTest(t, "al36_overwrite_xflags_cppflags.sh", "AL36")
}

func TestAccAL36OverwriteXflagsCxxflags(t *testing.T) {
	AccTest(t, "al36_overwrite_xflags_cxxflags.sh", "AL36")
}

func TestAccAL36OverwriteXflagsGoflags(t *testing.T) {
	AccTest(t, "al36_overwrite_xflags_goflags.sh", "AL36")
}

func TestAccAL36OverwriteXflagsCflagsCorrect(t *testing.T) {
	testFile := "al36_overwrite_xflags_cflags_correct.sh"
	f, err := os.Open(path.Join("testdata", testFile))
	require.NoError(t, err, "could not open testdata/%s", testFile)

	violations := linter.Check(f, testFile)
	require.Len(t, violations, 0, "AL36 should not trigger when the variable is present in the value (extending it)")
}

func TestAccAL49InvalidOption(t *testing.T) {
	AccTest(t, "al49_invalid_option.sh", "AL49")
}

func TestAccAL54MissingDefaultPrepare(t *testing.T) {
	AccTest(t, "al54_missing_default_prepare.sh", "AL54")
}

func TestAccAL57InvalidArch(t *testing.T) {
	AccTest(t, "al57_invalid_arch.sh", "AL57")
}

func TestAccAL61BadVersion(t *testing.T) {
	AccTest(t, "al61_bad_version.sh", "AL61")
}

func TestAccAL62VolatileSource(t *testing.T) {
	AccTest(t, "al62_volatile_source.sh", "AL62")
}
