exec >&2

find . -name '*.go' -exec redo-ifchange {} \;

go build -o "$3" cmd/ast/ast.go
