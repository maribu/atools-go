module alpinelinux.org/go/atools

go 1.21

toolchain go1.22.0

require (
	github.com/MakeNowJust/heredoc/v2 v2.0.1
	github.com/moznion/go-optional v0.11.0
	github.com/stretchr/testify v1.9.0
	gitlab.alpinelinux.org/alpine/go v0.10.0
	golang.org/x/exp v0.0.0-20240222234643-814bf88cf225
	mvdan.cc/sh/v3 v3.8.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
