pkgname=my-pkg
pkgver=1.2.3
pkgrel=0
pkgdesc="An example package"
url="https://example.com/example"
arch="all !x86"
license="MIT"
options="net !check"
makedepends="
	autotools
	bar-dev
	foo-dev
	baz
	"
depends="pkg"
checkdepends="check-pkg"
subpackages="$pkgname-dev $pkgname-doc foo::_foo ${pkgname}a"
source="$pkgname-$pkgver.tar.gz::example.com/downloads/example-$pkgver.tar.gz"

# secfixes:
#   1.2.1-r0:
#     - CVE-0000-00000

prepare() {
	default_prepare

	autoreconf -fi
}

build() {
	./configure \
		--prefix=/usr \
		--with-lib

	jobs=${jobs-2}
	make JOBS="$jobs"
}

check() {
	make check
}

package() {
	make install DESTDIR="$pkgdir"
}

sha512sums="
952de772210118f043a4e2225da5f5943609c653a6736940e0fad4e9c7cd3cfdd348abebbf28af7b4438c55515e5a351b87cc60c808673f4d23cf12237debf41 example-1.2.3.tar.gz
"
