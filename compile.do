exec >&2

redo-ifchange config.redo

command="go build"

. ./config.redo

if [ -n "$LDFLAGS" ]; then
    command="$command -ldflags '$LDFLAGS'"
fi

if [ -n "$GCFLAGS" ]; then
    command="$command -gcflags='$GCFLAGS'"
fi

command="$command -o \"\$2\" \"\$1\""

: >$3
printf "#!/bin/sh\n%s\n" "$command" >$3
chmod +x $3
