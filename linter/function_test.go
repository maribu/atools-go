package linter

import (
	"testing"

	"alpinelinux.org/go/atools/apkbuild"
	"github.com/MakeNowJust/heredoc/v2"
	"github.com/stretchr/testify/require"
)

func FunctionTestCaseSingleViolation(
	testcase string,
	t *testing.T,
	checkFunc func(*apkbuild.Apkbuild) []LintingViolation,
	expectedViolation LintingViolation,
) {

	file := apkbuild.MakeFile(heredoc.Doc(testcase))

	apkbuild, err := apkbuild.Parse(file, "APKBUILD")
	require.NoError(t, err)

	violations := checkFunc(apkbuild)

	require.NotEmpty(t, violations, "there should at least be one violation")
	require.True(t, violations[0].Is(expectedViolation), "expected violation %s, got %s", expectedViolation.Code, violations[0].Code)
}

func FunctionTestCaseNoViolations(
	testcase string,
	t *testing.T,
	checkFunc func(*apkbuild.Apkbuild) []LintingViolation,
) {
	file := apkbuild.MakeFile(heredoc.Doc(testcase))

	apkbuild, err := apkbuild.Parse(file, "APKBUILD")
	require.NoError(t, err)

	violations := checkFunc(apkbuild)

	require.Empty(t, violations, "unexpected violations received: %#v", violations)
}

func TestReturnOne(t *testing.T) {
	program := `
	func() {
		make || return 1
	}
	`

	FunctionTestCaseSingleViolation(program, t, CheckUnnecessaryReturnOne, AL2)
}

func TestReturnOneDoesNotWarnOnReturnZero(t *testing.T) {
	program := `
	func() {
		make || return 0
	}
	`

	FunctionTestCaseNoViolations(program, t, CheckUnnecessaryReturnOne)
}

func TestReturnOneDoesNotWarnOnNormalCommand(t *testing.T) {
	program := `
	func() {
		make
	}
	`

	FunctionTestCaseNoViolations(program, t, CheckUnnecessaryReturnOne)
}

func TestReturnOneCompound(t *testing.T) {
	program := `
	func() {
		make && make check || return 1
	}
	`

	FunctionTestCaseSingleViolation(program, t, CheckUnnecessaryReturnOne, AL2)
}

func TestReturnOneBraces(t *testing.T) {
	program := `
	func() {
		make || { echo "oops"; return 1; }
	}
	`

	FunctionTestCaseSingleViolation(program, t, CheckUnnecessaryReturnOne, AL2)
}

func TestFunctionsKeyword(t *testing.T) {
	program := `
	function a() {
		true
	}
	`

	FunctionTestCaseSingleViolation(program, t, CheckFunctionKeyword, AL9)
}

func TestFunctionsKeywordNormalFunction(t *testing.T) {
	program := `
	a() {
		true
	}
	`

	FunctionTestCaseNoViolations(program, t, CheckFunctionKeyword)
}

func TestSpaceBeforeFunctionParenthesis(t *testing.T) {
	program := `
	func () {
		true
	}
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckSpaceBeforeFunctionParenthesis,
		AL10,
	)
}

func TestSpaceBeforeFunctionParenthesisDoesNotMatchCorrectStyle(t *testing.T) {
	program := `
	func() {
		true
	}
	`

	VariableTestCaseNoViolations(
		program,
		t,
		CheckSpaceBeforeFunctionParenthesis,
	)
}

func TestSpaceAfterFunctionParenthesis(t *testing.T) {
	program := `
	func(){
		true
	}
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckSpaceAfterFunctionParenthesis,
		AL11,
	)
}

func TestSpaceAfterFunctionParenthesisDoubleSpace(t *testing.T) {
	program := `
	func()  {
		true
	}
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckSpaceAfterFunctionParenthesis,
		AL11,
	)
}

func TestOpeningNewlineBrace(t *testing.T) {
	program := `
	func()
	{
		true
	}
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckNewlineOpeningBrace,
		AL12,
	)
}

func TestOpeningNewlineBraceCorrect(t *testing.T) {
	program := `
	func() {
		true
	}
	`

	VariableTestCaseNoViolations(
		program,
		t,
		CheckNewlineOpeningBrace,
	)
}

func TestMissingDefaultPrepare(t *testing.T) {
	program := `
	prepare() {
		true
	}
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckMissingDefaultPrepare,
		AL54,
	)
}

func TestMissingDefaultPrepareWhenPresent(t *testing.T) {
	program := `
	prepare() {
		default_prepare
	}
	`

	VariableTestCaseNoViolations(
		program,
		t,
		CheckMissingDefaultPrepare,
	)
}
