package main

import (
	"fmt"
	"os"

	"alpinelinux.org/go/atools/linter"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Printf("Usage: %s <filename>\n", os.Args[0])
		os.Exit(1)
	}

	filename := os.Args[1]

	if _, err := os.Stat(filename); os.IsNotExist(err) {
		fmt.Printf("Error: File %s does not exist\n", filename)
		os.Exit(1)
	}

	file, err := os.Open(filename)

	if err != nil {
		fmt.Printf("%s\n", err)
		os.Exit(1)
	}

	violations := linter.Check(file, filename)

	for _, violation := range violations {
		fmt.Printf("%s\n", violation)
	}
}
