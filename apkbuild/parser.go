package apkbuild

import (
	"bytes"
	"fmt"
	"io"
	"strings"

	"alpinelinux.org/go/atools/internal/iterator"
	"github.com/moznion/go-optional"
	"mvdan.cc/sh/v3/syntax"
)

type Variable struct {
	Assignment     *syntax.Assign
	Name           string
	Value          string
	EvaluatedValue string
	Exported       bool
	Local          bool
	CommandSubst   []*syntax.CmdSubst
}

type VariableScope []Variable

func (v VariableScope) Get(name string) optional.Option[Variable] {
	result := optional.None[Variable]()

	for _, current := range v {
		if current.Name == name {
			result = optional.Some(current)
		}
	}

	return result
}

func (v VariableScope) GetValue(name string) string {
	return optional.MapOr(v.Get(name), "", func(v Variable) string { return v.EvaluatedValue })
}

// Iter calls the provided function for each variable and provides the variable
// to the function. If the function returns false, the iteration will stop.
func (v VariableScope) Iter(f func(Variable)) {
	for _, v := range v {
		f(v)
	}
}

// IterVar calls the provided function for each variable where the name matches
// provided name and provides the variable to the function. If the function
// returns false, the iteration will stop.
func (v VariableScope) IterVar(name string, f func(Variable)) {
	v.Iter(func(v Variable) {
		if v.Name == name {
			f(v)
		}
	})
}

type Apkbuild struct {
	File            *syntax.File
	GlobalVariables VariableScope
	Functions       []Function
	Lines           []string
}

func (a Apkbuild) GetGlobal(name string) string {
	return a.GlobalVariables.GetValue(name)
}

func Parse(f io.Reader, filename string) (apkbuild *Apkbuild, err error) {
	buf := bytes.Buffer{}
	teeReader := io.TeeReader(f, &buf)

	parsedFile, err := syntax.NewParser(
		syntax.Variant(syntax.LangBash),
	).Parse(teeReader, filename)
	if err != nil {
		return nil, err
	}

	apkbuild = &Apkbuild{
		File:  parsedFile,
		Lines: strings.Split(buf.String(), "\n"),
	}

	apkbuild.GlobalVariables = parseAssignments(parsedFile.Stmts)
	apkbuild.Functions = parseFunctions(parsedFile.Stmts)

	return
}

// IterAssigns will call the provided function for each assignment in the file,
// both in the global scope, as in each function.
func (a *Apkbuild) IterAssigns(iterFunc func(Variable)) {
	a.GlobalVariables.Iter(iterFunc)
	for _, f := range a.Functions {
		f.Variables.Iter(iterFunc)
	}
}

func (a *Apkbuild) AssignsIterator() iterator.Iterator[Variable] {
	return iterator.NewSliceIterator(a.GlobalVariables)
}

func (a *Apkbuild) FuncIterator() *iterator.SliceIterator[[]Function, Function] {
	return iterator.NewSliceIterator(a.Functions)
}

// parseAssignments takes a list of statements and extracts all variable
// assignments out of it. It recurses the AST, but it will not go into
// functions or subshells, which means it's limited to a single scope.
func parseAssignments(stmts []*syntax.Stmt) (variables VariableScope) {
	for _, stmt := range stmts {
		switch cmd := stmt.Cmd.(type) {
		case *syntax.CallExpr:
			if cmd.Assigns != nil {
				if len(cmd.Args) > 0 {
					// This is a command execution, not a variable assignment
					continue
				}
				for _, assignment := range cmd.Assigns {
					variable := parseAssignment(assignment, variables)
					variables = append(variables, variable)
				}
			}
		case *syntax.DeclClause:
			if cmd.Variant.Value != "export" && cmd.Variant.Value != "local" {
				continue
			}

			if cmd.Args != nil {
				for _, assignment := range cmd.Args {
					variable := parseAssignment(assignment, variables)
					variable.Exported = cmd.Variant.Value == "export"
					variable.Local = cmd.Variant.Value == "local"
					variables = append(variables, variable)
				}
			}
		case *syntax.IfClause:
			variables = append(variables, parseGlobalVariablesIfClause(cmd)...)
		case *syntax.CaseClause:
			for _, caseItem := range cmd.Items {
				variables = append(variables, parseAssignments(caseItem.Stmts)...)
			}
		case *syntax.ForClause:
			variables = append(variables, parseAssignments(cmd.Do)...)
		case *syntax.WhileClause:
			variables = append(variables, parseAssignments(cmd.Do)...)
		case *syntax.Block:
			variables = append(variables, parseAssignments(cmd.Stmts)...)
		}
	}
	return
}

func parseGlobalVariablesIfClause(ifClause *syntax.IfClause) (variables VariableScope) {
	variables = append(variables, parseAssignments(ifClause.Then)...)
	if ifClause.Else != nil {
		variables = append(variables, parseGlobalVariablesIfClause(ifClause.Else)...)
	}
	return
}

func parseAssignment(assignment *syntax.Assign, variables VariableScope) (v Variable) {
	var static, evaluated string
	var cmdsubsts []*syntax.CmdSubst
	if assignment.Value == nil {
		static = ""
		evaluated = ""
	} else {
		static, evaluated = parseWordParts(assignment.Value.Parts, variables)
		cmdsubsts = extractCmdSubst(assignment.Value.Parts)
	}

	v = Variable{
		Assignment:     assignment,
		Name:           assignment.Name.Value,
		Value:          static,
		EvaluatedValue: evaluated,
		CommandSubst:   cmdsubsts,
	}
	return
}

func parseWordParts(parts []syntax.WordPart, variables VariableScope) (static string, parsed string) {
	var staticBuilder strings.Builder
	var evaluatedBuilder strings.Builder

	for _, part := range parts {
		switch p := part.(type) {
		case *syntax.Lit:
			staticBuilder.WriteString(p.Value)
			evaluatedBuilder.WriteString(p.Value)
		case *syntax.SglQuoted:
			staticBuilder.WriteString(p.Value)
			evaluatedBuilder.WriteString(p.Value)
		case *syntax.DblQuoted:
			static, evaluated := parseWordParts(p.Parts, variables)
			staticBuilder.WriteString(static)
			evaluatedBuilder.WriteString(evaluated)
		case *syntax.ParamExp:
			staticBuilder.WriteString(fmt.Sprintf("$%s", p.Param.Value))
			variables.Get(p.Param.Value).IfSome(func(val Variable) {
				evaluatedBuilder.WriteString(val.EvaluatedValue)
			})
		case *syntax.CmdSubst:
			printer := syntax.NewPrinter()
			printer.Print(&staticBuilder, p)
			// We don't evaluate command substitutions, we add them as is.
			printer.Print(&evaluatedBuilder, p)
		}
	}

	return staticBuilder.String(), evaluatedBuilder.String()
}

func extractCmdSubst(parts []syntax.WordPart) (cmdsubsts []*syntax.CmdSubst) {
	for _, part := range parts {
		switch subpart := part.(type) {
		case *syntax.CmdSubst:
			cmdsubsts = append(cmdsubsts, subpart)
		case *syntax.DblQuoted:
			cmdsubsts = append(cmdsubsts, extractCmdSubst(subpart.Parts)...)
		}
	}
	return
}

type Function struct {
	Name      string
	FuncDecl  *syntax.FuncDecl
	Stmts     []*syntax.Stmt
	Variables VariableScope
}

// StmtsIterator returns an Iterator that returns each top-level statements in
// this function.
func (f Function) StmtsIterator() iterator.Iterator[*syntax.Stmt] {
	return iterator.NewSliceIterator(f.Stmts)
}

func parseFunctions(stmts []*syntax.Stmt) (functions []Function) {
	for _, stmt := range stmts {
		switch cmd := stmt.Cmd.(type) {
		case *syntax.FuncDecl:
			function := Function{
				Name:     cmd.Name.Value,
				FuncDecl: cmd,
			}

			if body, ok := cmd.Body.Cmd.(*syntax.Block); ok {
				function.Variables = parseAssignments(body.Stmts)
				function.Stmts = body.Stmts
			}

			functions = append(functions, function)
		}
	}
	return
}

func MakeFile(program string) (f io.Reader) {
	f = strings.NewReader(program)
	return
}

type Call struct {
	Command string
	Args    []string
	Expr    *syntax.CallExpr
}

func ParseCallExpr(expr *syntax.CallExpr) (call Call) {
	call.Expr = expr
	for _, word := range expr.Args {
		static, _ := parseWordParts(word.Parts, VariableScope{})

		if call.Command == "" {
			call.Command = static
		} else {
			call.Args = append(call.Args, static)
		}
	}

	return
}
